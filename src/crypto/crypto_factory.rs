use crate::crypto::Crypto;
use crate::crypto::CryptoFamily;
use crate::crypto::bcrypt::Bcrypt;

pub struct CryptoFactory;

impl CryptoFactory {
    pub fn new(family: CryptoFamily) -> impl Crypto {
        match family {
            CryptoFamily::MD5 => panic!(),
            CryptoFamily::SHA1 => panic!(),
            CryptoFamily::SHA256 => panic!(),
            CryptoFamily::SHA512 => panic!(),
            CryptoFamily::SHA3 => panic!(),
            CryptoFamily::BCRYPT(workfactor) => Bcrypt(workfactor)
        }
    }
}
