use crate::crypto::Crypto;

pub struct Bcrypt(pub u32);

impl Crypto for Bcrypt {
    fn compute(&self, input: &[u8]) -> String {
        bcrypt::hash(input, self.0).unwrap()
    }

    fn verify(&self, input: &[u8], hash: &[u8]) -> bool {
        false
    }
}
