pub mod bcrypt;
pub mod crypto_factory;

pub enum CryptoFamily {
    MD5,
    SHA1,
    SHA256,
    SHA512,
    SHA3,
    BCRYPT(u32)
}

pub trait Crypto {
    fn compute(&self, input: &[u8]) -> String;
    fn verify(&self, input: &[u8], hash: &[u8]) -> bool;
}
