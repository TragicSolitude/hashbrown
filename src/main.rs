#[macro_use]
extern crate clap;
extern crate bcrypt;

mod crypto;

use std::io;
use std::io::BufRead;
use clap::App;
use crypto::CryptoFamily;
use crypto::Crypto;
use crypto::crypto_factory::CryptoFactory;

fn main() {
    let cli_yml = load_yaml!("cli.yml");
    let app = App::from_yaml(cli_yml)
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"));

    let matches = app.get_matches();
    let is_quiet = matches.is_present("quiet");
    let intermediate: u8 = 0
        + 3 * matches.is_present("bcrypt") as u8
        + 5 * matches.is_present("sha1") as u8
        + 7 * matches.is_present("sha256") as u8
        + 11 * matches.is_present("sha512") as u8
        + 13 * matches.is_present("sha3") as u8;
    let mode: u8 = 0
        + 3 * matches.is_present("compute") as u8
        + 5 * matches.is_present("verify") as u8;
    let family = match intermediate {
        3 => {
            let workfactor = value_t!(matches, "workfactor", u32)
                .unwrap_or(bcrypt::DEFAULT_COST);

            CryptoFamily::BCRYPT(workfactor)
        },
        5 => CryptoFamily::SHA1,
        7 => CryptoFamily::SHA256,
        11 => CryptoFamily::SHA512,
        13 => CryptoFamily::SHA3,
        _ => unreachable!()
    };
    let crypto = CryptoFactory::new(family);
    let stdin = io::stdin();
    eprint!("Enter Value: ");
    for line in stdin.lock().lines() {
        let content = line.unwrap_or(String::new());
        if content.is_empty() {
            break;
        }

        match mode {
            3 => {
                println!("{}", crypto.compute(content.as_bytes()));
                if !is_quiet {
                    eprint!("Enter Value: ");
                }
            },
            5 => {
                println!("TODO");
            },
            _ => unreachable!()
        }
    }
}
